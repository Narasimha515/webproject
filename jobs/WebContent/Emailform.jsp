<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<style>
.navbar {
	margin-bottom: 0;
	background-color: maroon;
	z-index: 9999;
	border: 0;
	font-size: 18px !important;
	line-height: 1.42857143 !important;
	letter-spacing: 3px;
	border-radius: 0;
}

.navbar li a, .navbar .navbar-brand {
	color: white !important;
}

.navbar-nav li a:hover, .navbar-nav li.active a {
	color: #f4511e !important;
	background-color: green !important;
}

</style>

</head>
<body style="background-color: cyan">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#myPage"><h4>SENDING EMAIL </h4></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="Loginpage.jsp"><h4>BACK</h4></a></li>
			</ul>
		</div>
	</nav>
	<br>
	<br>
	<br>
	
	<div style="text-align: center">
			<h2>Sending Email Page</h2>
			<br>
		</div>
		<div style="text-align: center">
			<p><h3>Apply for the job</h3></p>
		</div>
             <div class="column">
		     	<div class="control-label col-sm-7" align="center">
		
				<form action="Emailformservlet" method="post">
					<table border="0" width="35%" align="center">

						<tr>
							<td width="50%">Enter Sender Email</td>
							<td><input type="text" name="email" size="50" /></td>
						</tr>
						<tr>
							<td>Subject</td>
							<td><input type="text" name="subject" size="50" /></td>
						</tr>
						<tr>
							<td>Content</td>
							<td><textarea rows="10" cols="39" name="content"></textarea>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center"><input type="submit"
								value="Send" /></td>
						</tr>

						<tr>
							<td colspan="2" align="center"><input type="submit"
								value="Reset" /></td>
						</tr>
					</table>
					</form>
			</div>
		</div>
		
	
</body>
</html>