<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <style>
.navbar {
	margin-bottom: 0;
	background-color: magenta;
	z-index: 9999;
	border: 0;
	font-size: 18px !important;
	line-height: 1.42857143 !important;
	letter-spacing: 3px;
	border-radius: 0;
}

.navbar li a, .navbar .navbar-brand {
	color: white !important;
}

.navbar-nav li a:hover, .navbar-nav li.active a {
	color: #f4511e !important;
	background-color: green !important;
}

</style>
   </head>
   <body style="background-color: orange">
    <nav class="navbar navbar-default navbar-fixed-top">
<div class="container">
<div class="navbar-header">
<a class="navbar-brand" href="#myPage"><h4>PERSONAL</h4></a>
</div>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="Home.html"><h4>BACK</h4></a></li>
		</ul>
		</div>
		</nav><br><br><br><br> 
    
          
        <form action ="RegistrationServlet" method="post"class="form-horizontal">
            <div   class="form-group">
                <label class="control-label col-sm-4" for="Name">Name:</label>
                <div class="col-sm-4">
                    <input type="Enterusername" class="form-control" id="Enterusername" placeholder="Enterusername"name="Enterusername">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="password">EmailID:</label>
                <div class="col-sm-4">
                    <input type="Email" class="form-control" id="Email" placeholder="Enter valid EmailID" name="Email">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="rpwd">Create Password:</label>
                <div class="col-sm-4">
                    <input type="password" class="form-control" id="password" placeholder="Minimum 6character"name="password">
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-sm-4" for="number">Mobile number:</label>
                <div class="col-sm-4">
                    <input type="Entermobilenumber" class="form-control" id="number" placeholder="Entermobilenumber"name="number">
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-sm-4" for="City">Current location:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="cities">
                        <option value="Kadapa">Select</option>
                            <option value="Kadapa">Kadapa</option>
                            <option value="Chennai">Chennai</option>
                            <option value="Banga">Bangalore</option>
                            <option value="Anthapur">Ananthapur</option>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-sm-4" for="Educational">Highest Qualification:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Qualification">
                       <option value="Kadapa">Select</option>
                            <option value="Graduation">Masters/Post-Graduation</option>
                            <option value="Graduation">Graduation/Deploma</option>
                            <option value="Graduation">12th</option>
                            <option value="Graduation">10th</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="Course">Course:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Course">
                       <option value="Course">Select your course</option>
                            <option value="Course">B.A</option>
                            <option value="Course">B.com</option>
                            <option value="Course">BSC</option>
                            <option value="Course">B.Tech/BE</option>
                            <option value="Course">MBBS</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="Specialization">Specialization:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Specialization">
                       <option value="Specialization">Specialization</option>
                            <option value="Specialization">Civil</option>
                            <option value="Specialization">Computers</option>
                            <option value="Specialization">Electrical</option>
                            <option value="Specialization">Environmental</option>
                        </select>
                    </div>
                </div>
            </div>
                <div class="form-group">
                <label class="control-label col-sm-4" for="password">University/College:</label>
                <div class="col-sm-4">
                    <input type="University" class="form-control" id="University" placeholder="Institute Name" name="University">
                </div>
            </div>
                <div class="form-group">
                <label class="control-label col-sm-4" for="promotion">Course Type:</label>
                <div class="col-sm-4">
                    <div class="form-check">
                        <label><input type="radio"  id=Course" name="CourseType" checked>full time</label>
                        <label><input type="radio"  id="Course" name="CourseType" >Part time</label>
                     </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="Specialization">Passing year:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Passing">
                       <option value="Passing">Select</option>
                            <option value= 2020">2020</option>
                            <option value= 2019>2019</option>
                            <option value= 2018>2018</option>
                            <option value= 2017>2017</option>
                            <option value= 2016 >2016</option>
                            <option value= 2015 >2015</option>
                            <option value= 2014 >2014</option>
                        </select>
                    </div>
                </div>
            </div>
            
            
                <div class="form-group">
                    <label class="control-label col-sm-4" for="Resume">UplodResume:</label>
                    <div class="col-sm-4">
                        <div class="checkbox">
                            <input type="file">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                <label class="control-label col-sm-4" for="course"></label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <label><input type="checkbox" name="conditions">I agreed to the Terms and Conditions governing the use of ONLINE JOB PORTAL.com </label>
                        
                    </div>
                </div>
            </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-4">
                        <input type="submit"value="submit" />    
                                        
                </div>
                </div>
        </form>
    </div>
</body>
   
</html>

