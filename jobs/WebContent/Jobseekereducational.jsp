<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    
   </head>
   <body>
      <div class="navbar" style="background-color:lightgreen">
         <center><a href="#welcome"><h2>Educational </h2></a></center>
      </div>
        
    <div class="container">
           
        <form class="form-horizontal" name="registration">
            <div class="form-group">
                <label class="control-label col-sm-4" for="Educational">Highest Qualification:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Qualification">
                       <option value="Kadapa">Select</option>
                            <option value="Graduation">Masters/Post-Graduation</option>
                            <option value="Graduation">Graduation/Deploma</option>
                            <option value="Graduation">12th</option>
                            <option value="Graduation">10th</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="Course">Course:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Course">
                       <option value="Course">Select your course</option>
                            <option value="Course">B.A</option>
                            <option value="Course">B.com</option>
                            <option value="Course">BSC</option>
                            <option value="Course">B.Tech/BE</option>
                            <option value="Course">MBBS</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="Specialization">Specialization:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Specialization">
                       <option value="Specialization">Specialization</option>
                            <option value="Specialization">Civil</option>
                            <option value="Specialization">Computers</option>
                            <option value="Specialization">Electrical</option>
                            <option value="Specialization">Environmental</option>
                        </select>
                    </div>
                </div>
            </div>
                <div class="form-group">
                <label class="control-label col-sm-4" for="password">University/College:</label>
                <div class="col-sm-4">
                    <input type="University" class="form-control" id="University" placeholder="Institute Name" name="University">
                </div>
            </div>
                <div class="form-group">
                <label class="control-label col-sm-4" for="promotion">Course Type:</label>
                <div class="col-sm-4">
                    <div class="form-check">
                        <label><input type="radio"  id=Course" name="Course" checked>full time</label>
                        <label><input type="radio"  id="Course" name="Course" >Part time</label>
                     </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="Specialization">Passing year:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Passing">
                       <option value="Passing">Select</option>
                            <option value="Passing">2020</option>
                            <option value="Passing">2019</option>
                            <option value="Passing">2018</option>
                            <option value="Passing">2017</option>
                            <option value="Passing">2016</option>
                            <option value="Passing">2015</option>
                            <option value="Passing">2014</option>
                        </select>
                    </div>
                </div>
            </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-4">
                        <button type="button"style="background-color:lightblue"class="btn btn-default" onclick="formValidation();">Continue</button>
                    </div>
                </div>
        </form>
    </div>
</body>
   
</html>

