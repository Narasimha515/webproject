<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<style>
.navbar {
	margin-bottom: 0;
	background-color: maroon;
	z-index: 9999;
	border: 0;
	font-size: 18px !important;
	line-height: 1.42857143 !important;
	letter-spacing: 3px;
	border-radius: 0;
}

.navbar li a, .navbar .navbar-brand {
	color: white !important;
}

.navbar-nav li a:hover, .navbar-nav li.active a {
	color: #f4511e !important;
	background-color: green !important;
}

.imgcontainer {
	text-align: center;
	margin: 24px 0 12px 0;
}
</style>
</head>
<body style="background-color: cyan">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#myPage"><h4>LOGINPAGE</h4></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="Home.html"><h4>BACK</h4></a></li>
			</ul>
		</div>
	</nav>
	<br>
	<br>
	<br>
	<br>
	<form action="LoginServlet1" method="post" class="form-horizontal">
		<div class="imgcontainer">
			<img src="loginimage.png" alt="Avatar" class="avatar">
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4" for="email">Username:</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" id="name"
					placeholder="Enter username" name="UserName">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4" for="pwd">EmailID or
				Password:</label>
			<div class="col-sm-4">
				<input type="password" class="form-control" id="pwd"
					placeholder="Enter valid emailID or password" name="Password">
			</div>
		</div>
		<div class="checkbox">
			<div class="col-sm-offset-4 col-sm-4">
				<label><input type="radio" name="radio" value="JobSeeker"
					selected> Jobseeker</label> <label><input type="radio"
					name="radio" value="JobProvider"> Jobprovider</label>
			</div>
		</div>
		<br>
		<br>
		<div class="form-group">
			<div class="col-sm-offset-4 col-sm-4">
				<center>
					<button type="submit" class="btn btn-default">Login</button>
				</center>
			</div>
		</div>

	</form>
	</div>

</body>

</html>
