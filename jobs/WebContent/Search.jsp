<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <style>
.navbar {
	margin-bottom: 0;
	background-color: deepskyblue;
	z-index: 9999;
	border: 0;
	font-size: 18px !important;
	line-height: 1.42857143 !important;
	letter-spacing: 3px;
	border-radius: 0;
}

.navbar li a, .navbar .navbar-brand {
	color: white !important;
}

.navbar-nav li a:hover, .navbar-nav li.active a {
	color: #f4511e !important;
	background-color: green !important;
}

.imgcontainer {
	text-align: center;
	margin: 24px 0 12px 0;
}
</style>
    
</head>
<body style="background-color: chartreuse">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#myPage"><h4>SEARCH</h4></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="UpdateCompany.jsp"><h4>BACK</h4></a></li>
			</ul>
		</div>
	</nav>
	<br><br><br><br><br>
	     <form action ="SearchViewPage.jsp" method="post"class="form-horizontal">
        <div class="form-group">
                <label class="control-label col-sm-4" for="Course">Course:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Course">
                       <option value="Course">Select your course</option>
                            <option value="Course">B.A</option>
                            <option value="Course">B.com</option>
                            <option value="Course">BSC</option>
                            <option value="Course">B.Tech/BE</option>
                            <option value="Course">MBBS</option>
                        </select>
                    </div>
                </div>
            </div>
             <div class="form-group">
                <label class="control-label col-sm-4" for="Specialization">Specialization:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Specialization">
                       <option value="Specialization">Specialization</option>
                            <option value="Specialization">Civil</option>
                            <option value="Specialization">Computers</option>
                            <option value="Specialization">Electrical</option>
                            <option value="Specialization">Environmental</option>
                        </select>
                    </div>
                </div>
            </div>
             <div class="form-group">
                <label class="control-label col-sm-4" for="City">Current location:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="cities">
                        <option value="Kadapa">Select</option>
                            <option value="Kadapa">Kadapa</option>
                            <option value="Chennai">Chennai</option>
                            <option value="Banga">Bangalore</option>
                            <option value="Anthapur">Ananthapur</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="Specialization">Passing year:</label>
                <div class="col-sm-4">
                    <div class="checkbox">
                        <select name="Passing">
                       <option value="Passing">Select</option>
                            <option value= 2020">2020</option>
                            <option value= 2019>2019</option>
                            <option value= 2018>2018</option>
                            <option value= 2017>2017</option>
                            <option value= 2016 >2016</option>
                            <option value= 2015 >2015</option>
                            <option value= 2014 >2014</option>
                        </select>
                    </div>
                </div>
            </div>
             <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-4">
                        <input type="submit"value="Search" />    
                                        
                </div>
                </div>
            </form>
            </div>


</body>
</html>