<%@ page import="java.util.Base64"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%! String driver = "oracle.jdbc.driver.OracleDriver";%>
<%!String url = "jdbc:oracle:thin:@localhost:1521:orcl";%>
<%!String username = "jobportal";%>
<%!String password = "2039";%>
<%
   try {
   Class.forName(driver);
   } catch (ClassNotFoundException e) {
   e.printStackTrace();
   }
   Connection cn = null;
   Statement statement = null;
   ResultSet resultSet = null;
   %>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<style>
.navbar {
	margin-bottom: 0;
	background-color: hotpink;
	z-index: 9999;
	border: 0;
	font-size: 18px !important;
	line-height: 1.42857143 !important;
	letter-spacing: 3px;
	border-radius: 0;
}

.navbar li a, .navbar .navbar-brand {
	color: white !important;
}

.navbar-nav li a:hover, .navbar-nav li.active a {
	color: #f4511e !important;
	background-color: green !important;
}

</style>
	

</head>
<body style="background-color: lime">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#myPage"><h4>SEARCH RESULTS</h4></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="Loginpage.jsp"><h4>BACK</h4></a></li>
			</ul>
		</div>
	</nav>
	<br>
	<br>

		<div class="column">
			<div class="control-label col-sm-6" align="top">
				<br></br>
				<table border="1">
					<tr>
						<td>
							<h4>COMPANY_ID</h4>
						</td>
						<td>
							<h4>COMPANY_NAME</h4>
						</td>
						<td>
							<h4>JOB_TITLE</h4>
						</td>
						<td>
							<h4>DESCRIPTION</h4>
						</td>
						<td>
							<h4>LOCATION_NAME</h4>
						</td>
						<td>
							<h4>NUMBEROFPOST</h4>
						</td>
						<td>
							<h4>APPLY</h4>
						</td>
					</tr>
					<%
            try{
            	
            cn = DriverManager.getConnection(url,username,password);
            statement=cn.createStatement();
            String JobTitle = request.getParameter("Jobtitle");
            String Location = request.getParameter("Location");
            //System.out.println("JOB_TITLE" + JOB_TITLE);
            //System.out.println("Location" + Location);
            String sql ="select * from jobs where JOB_TITLE like '%"+JobTitle+"%' and LOCATION_NAME like '%"+Location+"%'";
            //System.out.println(sql);
            resultSet = statement.executeQuery(sql);
            while(resultSet.next()){
            	
            %>
					<tr>
						<td><%=resultSet.getString("COMPANY_ID") %></td>
						<td><%=resultSet.getString("COMPANY_NAME") %></td>
						<td><%=resultSet.getString("JOB_TITLE") %></td>
						<td><%=resultSet.getString("DESCRIPTION") %></td>
						<td><%=resultSet.getString("LOCATION_NAME") %></td>
						<td><%=resultSet.getString("NUMBEROFPOST") %></td>
						<td><a
							href="Emailform.jsp?id=<%=resultSet.getString("COMPANY_ID")%>">Apply</a></td>

					</tr>
					<%
            }
            cn.close();
            } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());

            }
            %>
				</table>
			</div>
		</div>
	</div>




</body>
</html>