<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <style>
.navbar {
	margin-bottom: 0;
	background-color: hotpink;
	z-index: 9999;
	border: 0;
	font-size: 18px !important;
	line-height: 1.42857143 !important;
	letter-spacing: 3px;
	border-radius: 0;
}

.navbar li a, .navbar .navbar-brand {
	color: white !important;
}

.navbar-nav li a:hover, .navbar-nav li.active a {
	color: #f4511e !important;
	background-color: green !important;
}

</style>
    
</head>
<body style="background-color: lime">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#myPage"><h4>ADD COMPANY</h4></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="Home.html"><h4>BACK</h4></a></li>
			</ul>
		</div>
	</nav>
	<br><br><br><br><br><br>

		<form action="companyupdateservlet" method="post"
			class="form-horizontal">

			<div class="form-group">
				<label class="control-label col-sm-4" for="password">Companyname:</label>
				<div class="col-sm-4">
					<input type="name" class="form-control" id="name" placeholder="Enter Companyname" name="name">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4" for="rpwd"> Password:</label>
				<div class="col-sm-4">
					<input type="password" class="form-control" id="password" placeholder="Enter Password" name="password">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4" for="number">Location:</label>
				<div class="col-sm-4">
					<input type="Location" class="form-control" id="Location" placeholder="EnterLocation" name="Location">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4" for="password">CompanyEmail:</label>
				<div class="col-sm-4">
					<input type="Email" class="form-control" id="Email" placeholder="Enter valid EmailID" name="Email">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-4">
					<input type="submit" value="update" />
				</div>
			</div>
		</form>
	</div>

</body>
</html>