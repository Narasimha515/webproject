<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<style>
.navbar {
	margin-bottom: 0;
	background-color: green;
	z-index: 9999;
	border: 0;
	font-size: 18px !important;
	line-height: 1.42857143 !important;
	letter-spacing: 3px;
	border-radius: 0;
}

.navbar li a, .navbar .navbar-brand {
	color: white !important;
}

.navbar-nav li a:hover, .navbar-nav li.active a {
	color: #f4511e !important;
	background-color: green !important;
}

</style>
</head>
<body style="background-color: white">
	<nav class="navbar navbar-default navbar-fixed-top">
<div class="container">
<div class="navbar-header">
<a class="navbar-brand" href="#myPage"><h4>CONTACT US</h4></a>
</div>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="Home.html"><h4>BACK</h4></a></li>
		</ul>
		</div>
		</nav><br><br><br><br>
		

		<form action="ContactSuccess.jsp"  class="form-horizontal">
			<div id="contact" class="container-fluid bg-grey">
				<h2 class="text-center">CONTACT</h2>
				<div class="row">
					<div class="col-sm-5">
						<p>Contact us and we'll get back to you within 24 hours.</p>
						<p>
							<span class="glyphicon glyphicon-map-marker"></span> Chennai, TN
						</p>
						<p>
							<span class="glyphicon glyphicon-phone"></span> +7702300436
						</p>
						<p>
							<span class="glyphicon glyphicon-envelope"></span>
							naga@gmail.com.com
						</p>
					</div>
					<div class="col-sm-7">
						<div class="row">
							<div class="col-sm-6 form-group">
								<input class="form-control" id="name" name="name"
									placeholder="Name" type="text" required>
							</div>
							<div class="col-sm-6 form-group">
								<input class="form-control" id="email" name="email"
									placeholder="Email" type="email" required>
							</div>
						</div>
						<textarea class="form-control" id="comments" name="comments"
							placeholder="Comment" rows="5"></textarea>
						<br>
						<div class="row">
							<div class="col-sm-12 form-group">
								<button class="btn btn-default pull-right"type="submit">Send</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	
	<div class="col-sm-12">                   
			<div class="thumbnail">
				<img src="contactuslogo.jpg" style="width:100%" height="100%">
			</div>
		</div>
	

</body>
</html>