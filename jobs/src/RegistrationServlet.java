

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 * Servlet implementation class RegistrationServlet
 */
@WebServlet("/RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("in post");
		try {

	     	Class.forName("oracle.jdbc.driver.OracleDriver");
		    Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","jobportal","2039");
			System.out.println("after connection");	
	 
			 PreparedStatement st = cn.prepareStatement("insert into jobseekerregistration values  (jobseq.nextval,?,?,?,?,?,?,?,?,?,?,?)");
			 st.setString(1,request.getParameter("Enterusername"));
		      st.setString(2,request.getParameter("Email"));
		      st.setString(3,request.getParameter("password"));
		      //st.setString(4,request.getParameter("number"));
		      String contact = request.getParameter("number");
		      System.out.println("contact " + contact);
		       int contactNumber=Integer.parseInt(contact);
		       System.out.println("number " + contactNumber);
		       
		       st.setInt(4,contactNumber);
		      st.setString(5,request.getParameter("cities"));
		      st.setString(6,request.getParameter("Qualification"));
		      st.setString(7,request.getParameter("Course"));
		      st.setString(8,request.getParameter("Specialization"));
		      st.setString(9,request.getParameter("University"));
		      st.setString(10,request.getParameter("CourseType")); 
		      String y = request.getParameter("Passing");
		      System.out.println("pass year is " +y);
		      int passYear = Integer.parseInt(request.getParameter("Passing"));
		      System.out.println("pass year is " +passYear);
		      st.setInt(11,passYear);
		   
		     
		   	 
		   int i = st.executeUpdate();
		 //  if(i != 0)
			   if (i != 0) {
				   response.sendRedirect("Home.html");
				   }
				   st.close();
				   } catch (Exception e) {
				   System.out.println(e.getMessage());
				   }}}