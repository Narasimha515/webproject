

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;


/**
 * Servlet implementation class UpdateCompany
 */
@WebServlet("/UpdateCompany")
public class UpdateCompany extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCompany() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("in post");
		try { 

	     	Class.forName("oracle.jdbc.driver.OracleDriver");
		    Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","jobportal","2039");
			System.out.println("after connection");	
	 
			 PreparedStatement st = cn.prepareStatement("insert into company values (companyseq.nextval,?,?,?,?)");
			 st.setString(1,request.getParameter("Company"));
			 st.setString(2,request.getParameter("password"));
		      st.setString(3,request.getParameter("Location"));
		      st.setString(4,request.getParameter("Email"));
		   
		   
		     
		   	 
		   int i = st.executeUpdate();
		   if(i != 0)
		   {
		   JOptionPane.showMessageDialog(null,"post added sucessfully","Welcome",JOptionPane.PLAIN_MESSAGE);
			
		   System.out.println("added ");
		   }
		   else 
		   {
		   JOptionPane.showMessageDialog(null,"post Not added Successfully","Error",JOptionPane.ERROR_MESSAGE);	
		   System.out.println("in else");
		   }
		st.close();
		cn.close();
		 PrintWriter out=response.getWriter();
		 out.print("<html><body><b>insert successfully</b></body></html>");
		
		}
		
		
		  catch(ClassNotFoundException e)
		  {
		  System.out.println(e.getMessage());
		  } 
		catch (SQLException e) {
		// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		  
		}
		
	}
